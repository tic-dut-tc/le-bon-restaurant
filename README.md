# ![resto](images/resto.png) **Le Bon Restaurant**

**Le but de ce TP est de pouvoir calculer automatiquement dans quel restaurant vous irez manger suivant les souhaits de vos amis qui vous accompagnent, afin que tout le monde soit satisfait.**


# **À savoir**

Les fonctions ``ET()``, ``OU()`` renvoie VRAI ou FAUX (à l'affichage).

La syntaxe de ces fonctions est de la forme suivante:

+ ``=ET("question1", "question2",...)``

+ ``=OU("question1", "question2",...)``

> **Ces fonctions doivent contenir au moins 2 conditions (2 questions).**
**La fonction ET() renvoie VRAI si toutes les réponses aux questions sont vraies.**
**La fonction OU() renvoie VRAI si au moins une des réponses aux questions est vraie.**

# ![](images/simulation.png) **La simulation**

Récupérez le fichier [**resto.xls**](fichiers/resto.xlsx).

Grâce aux critères de notations de chaque éventuel participant à la soirée, vous allez : 

+ Pour chaque participant calculer la note qu’il donne à chaque restaurant (tableau sur la feuille 2 de votre fichier);
+ Attention, quand  il  met  0  ceci  signifie  qu’il  «élimine»  le  restaurant  (donc  mettre «éliminé») et dans le total mettez «x» pour tout resto éliminé.
+ Vous  calculez  ensuite  toutes  les  combinaisons  possibles  en  fonction  des  jours  et  des convives (sur la feuille 3 de votre fichier) 
    - la  note  de  chaque  restaurant  sera  égale  à  la  somme  des  notes  données  par chaque  convive  avec  «x»  pour  les  restaurants  éliminés  par  au  moins  un  des convives,
    - pour   chaque   combinaison   de   convives,   vous   devez   faire   un   tableau récapitulatif indiquant quel restaurant est choisi et avec combien de points.


> Essayez de  regrouper  les  informations  afin  de  simplifier  les  formules  ou  au  moins  d’en réduire le nombre. 
> Fonctions utilisées: et, ou , si , nb.si, max , somme, recherchev

# ![](images/michelinlogo.png) **Les critères de notations**

## ![](images/mathieu.png) **Mathieu**
+ 2  si  le  repas  coûte  moins  de  15  euros,  
+ 1  sinon  à  moins  qu’il  n’y  ait  une  terrasse  (2 points aussi)
+ 2 point s’ils servent après 22h30
+ 1 point s’ils acceptent les groupes
+ par contre vu qu’il déteste l’odeur du fromage .. les restaurants de montagne auront 0

## ![](images/sidious.png) **Claude**
+ 2 points si le restaurant est ouvert le dimanche
+ 2 s’ils acceptent les groupes
+ moins 2 points s’il n’y a pas de terrasse
+ plus 3 points s’ils servent après 22h30

## ![](images/thomas.png) **Thomas**

+ scandinave : 2 points
+ libanais : 2 points
+ français : 1 point
+ montagne : 1 point
+ attention  il est  normand  et  ne  supporte  pas  les  Bretons  =>  refuse  d’aller  dans  une crêperie et met 0 à ce type de restaurant
+ asiatique : 1
+ italien : 2
+ grec : 5
+ latino : 1
+ indien : 1
+ pour  les  6  derniers  critères:  si  les  6  critères  sont  positifs:  6  points;  si  5  critères:  5 points, etc.


## ![](images/leia.png) **Agnès**

+ refuse d’aller dans un restaurant qui ne sert pas de la cuisine française (=> 0)
+ étant  étudiante,  le  tarif  compte  beaucoup  ...  elle  met  donc  1  point  si  le  repas  coûte plus de 15 euros et 2 points sinon
+ de  plus  elle  accorde  2  points  supplémentaires  aux  restaurants  qui  font  une  réduction pour les étudiants. 
+ Enfin, elle aime manger dehors et ajoute 2 points pour la terrasse

## ![](images/auriane.png) **Auriane**

+ ajoute 2 points si réduction étudiant
+ enlève 3 points si pas d’accès handicapés
+ ajoute 2 points si restaurant français
+ enlève 1 point si repas à plus de 15 euros

## ![](images/virginie.png) **Virginie**


+ 3 points si cuisine non française
+ mais 3 points aussi si cuisine française ouvert le dimanche et acceptant les groupes, 2 si  une  seule  de  ces  deux  dernières  conditions  est  satisfaite  ...  1  seul  point  dans  les autres cas.


# **Les présents en fonction des jours** 

**Lundi:** les 6 participants sont présents. 

**Mardi:** Mathieu, Agnès, Auriane et Virginie

**Mercredi:** Claude, Thomas, Auriane et Virginie

**Jeudi:** Caude, Thomas, Mathieu et Agnès

**Vendredi:** Claude, Mathieu, Auriane et Virginie

**Samedi:** Claude et Virginie

**Faites un tableau récapitulatif avec le jour, les présents, le nombre de points et le restaurant.**

